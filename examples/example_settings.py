# -*- coding: utf-8 -*-
"""AgileWS test settings file
"""

WSGI = {
    "server": "tornado",
    "port": 8082,
    "host": "0.0.0.0",
    "reloader": True
}

DEBUG = True


CONNECTIONS = {
    "pgbeast": {
        "driver": 'psycopg2',
        "host": 'localhost',
        "port": 5432,
        "database": "geotag",
        "user": "pav",
        "password": "1234"
    }
}


CACHES = {}