# -*- coding: utf-8 -*-
"""
**agilews.default_settings module**

This is the default configuration of AgileWS WSGI server.
It uses bottle.py build-in wsgi-adaptor support for
different backends.

Attributes::
    - PORT (int): Service listening port.
    - HOST (str): Service host IP or domain name.
    - SERVER (str): WSGI server backend.
    - DEBUG (bool): Debug mode flag.
    - RELOAD (bool): Reload mode for WSGI server.
    - CONNECTIONS (dict): Dictionary defining database connections
    - CACHES (dict): Dictionary defining cache backends connections
    - SOCKET (str): Service listening socket path (default - None).

::


.. note::
    You can actually extend base configuration and add your own parameters
    controlling connections, 3rd-party services, secret keys, oauth schemes etc.


Module documentation
++++++++++++++++++++
"""

WSGI = {
    "server": "tornado",
    "port": 8081,
    "host": "0.0.0.0",
    "reloader": True
}

DEBUG = True


CONNECTIONS = {
    "pgbeast": {
        "driver": 'psycopg2',
        "host": 'localhost',
        "port": 5432,
        "database": "geotag",
        "user": "pav",
        "password": "1234"
    }
}


CACHES = {}