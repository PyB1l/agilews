# -*- coding: utf-8 -*-
"""
Provides a routing mechanism for agilews class-based HTTPHandlers.

Features:
    - Auto method routing.
    - Auto ``options`` method binding.


Module documentation
++++++++++++++++++++
"""

__all__ = ['handler_dispatch']

from agilews.response import WSResponse
from collections import defaultdict
import functools


_options_method = lambda option_data, **kwargs: WSResponse.ok(option_data)


def _reverse_indexing(dict_obj):
    """Simple map-reduce function:

        Example::

            >>> data = {"a": 1, "b": 1, "c": 2}
            >>> print _reverse_indexing(data)
            {1: ["a", "b"], 2: ["c"]}
    """
    reversed_dict = defaultdict(list)
    for key, value in sorted(dict_obj.iteritems()):
        reversed_dict[value].append(key.upper())
    return dict(reversed_dict)


def handler_dispatch(base_app, handler_cls):
    """BaseHandler dispatcher function
    """

    options_routes = _reverse_indexing(handler_cls.handler_route)

    for route in options_routes:
        base_app.route(route, method='OPTIONS')(
            functools.partial(
                _options_method,
                option_data=options_routes.get(route)
            )
        )

    for method in handler_cls.handler_route:
        base_app.route(
            handler_cls.handler_route.get(method),
            method=[method.upper()]
        )(getattr(handler_cls, method.lower()))

    return True