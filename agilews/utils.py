# -*- coding: utf-8 -*-
"""
Provides a routing mechanism for agilews class-based HTTPHandlers.
type or HTTP method available. The skeleton looks like this::


.. note::
    In beta version only JSON renderer is supported. Must implement
    additional renderer classes (SOAP, XML, YAML).


Module documentation
++++++++++++++++++++
"""

from agilews import (ws_response, ws_request, WSResponse)


__all__ = ['cors_enable_hook', 'json_validator', 'type_validator']


def cors_enable_hook():
    ws_response.headers['Access-Control-Allow-Origin'] = '*'
    ws_response.headers['Access-Control-Allow-Headers'] = \
        'Authorization, Credentials, X-Requested-With, Content-Type'
    ws_response.headers['Access-Control-Allow-Methods'] = \
        'GET, PUT, POST, OPTIONS, DELETE'


def json_validator(callback):
    """Auto-validates json input
    """
    def _json_validator(*args, **kwargs):
        try:
            assert ws_request.json != ''
        except (ValueError, AssertionError):
            return WSResponse.bad_request('Invalid JSON request data.')
        return callback(*args, **kwargs)
    return _json_validator


def type_validator(**rules):
    """Validate key-value JSON input.
    """
    def _type_validator(callback):
        def __type_validator(*args, **kwargs):
            print ws_request.json, rules
            data = ws_request.json
            try:
                assert all([isinstance(data[key], rules[key]) for key in data])
            except AssertionError:
                return WSResponse.bad_request(
                    {"error": "Invalid types/keywords for {} params."
                     .format(str(rules.keys()))}
                )
            return callback(
                *args,
                **{key: rules.get(key)(data[key]) for key in data}
            )
        return __type_validator

    return _type_validator
