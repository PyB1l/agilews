# -*- coding: utf-8 -*-
"""Testing suite for example_service app.
"""

from webtest import TestApp
from example_service import webservice


class TestService(object):
    """Testing suite for agilews default behavior.
    """

    @classmethod
    def setup_class(cls):
        cls._app = TestApp(webservice)

    @classmethod
    def teardown_class(cls):
        del cls._app

    def test_ping(self):
        resp = self._app.get('/ping')

        print resp.json
        print resp.request

        assert resp.content_type == 'application/json'
        assert resp.status == '200 OK'
        assert resp.json['data']['service'] == "pong"
