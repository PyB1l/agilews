# -*- coding: utf-8 -*-
"""
Provides a routing mechanism for agilews class-based HTTPHandlers.
type or HTTP method available. The skeleton looks like this::


.. note::
    In beta version only JSON renderer is supported. Must implement
    additional renderer classes (SOAP, XML, YAML).


Module documentation
++++++++++++++++++++
"""

import json
from agilews import (ws_response, ws_request)
from agilews import webservice
from response import WSResponse

__all__ = ['error404', 'error405', 'error500']


@webservice.error(404)
def error404(errors):
    ws_response.content_type = 'application/json'
    ws_response._status_line = '200 OK'
    return json.dumps(WSResponse.not_found(
        [{"msg": "Model does not exists", "debug":
            {"endpoints": ['/ping']}}]
    ))


@webservice.error(405)
def error405(errors):
    ws_response.content_type = 'application/json'
    ws_response._status_line = '200 OK'
    allowed_methods = [f.method for f in filter(
        lambda x: x.rule == ws_request.path,
        webservice.routes)
    ]
    print allowed_methods
    return json.dumps(WSResponse.method_not_allowed(
        [{"msg": "{} method is not allowed".format(ws_request.method),
          'debug': {'allowed_methods': allowed_methods}}]
    ))


@webservice.error(500)
def error500(errors):
    ws_response.content_type = 'application/json'
    ws_response._status_line = '200 OK'
    return json.dumps(WSResponse.service_unavailable())
