# -*- coding: utf-8 -*-
"""Testing suite for agilews.response module.

.. note::
    Must have ``nose`` installed in order to run tests!
"""

from agilews.response import WSResponse
from nose.tools import assert_equals


response_tpl = None  # Just for editor pep8 warnings catching!


def setup_module(module):
    """Setup a Response prototype object.
    """
    print ("\nSetting prototype response template fixture in module.\n")
    module.response_tpl = {
        "status_code": 200,
        "status_text": "OK",
        "data": None,
        "errors": None
    }


def teardown_module(module):
    """Delete Response prototype object.
    """
    print "\nDestroy testing fixtures."
    del module.response_tpl


def test_response_attrs():
    """Testing WSResponse class for spec attributes.
    """
    attrs = ('errors', 'data', 'status_code')
    assert all((hasattr(WSResponse, attr) for attr in attrs))


def test_response_ok():
    """Testing WSResponse class for HTTP 200 ``OK`` status.
    """
    response_tpl['data'] = "Hello!"
    response_tpl['errors'] = []
    assert_equals(WSResponse.ok('Hello!'), response_tpl)


def test_response_created():
    """Testing WSResponse class for HTTP 201 ``Created`` status.
    """
    response_tpl['status_code'] = 201
    response_tpl['status_text'] = 'Created'
    response_tpl['data'] = None
    assert_equals(WSResponse.created(), response_tpl)


def test_response_bad_request():
    """Testing WSResponse class for HTTP 400 ``Bad Request`` status.
    """
    response_tpl['status_code'] = 400
    response_tpl['status_text'] = 'Bad Request'
    response_tpl['data'] = None
    response_tpl['errors'] = [{"msg": "invalid input."}]
    assert_equals(WSResponse.bad_request([{"msg": "invalid input."}]),
                  response_tpl)


def test_response_unauthorized():
    """Testing WSResponse class for HTTP 401 ``Unauthorized`` status.
    """
    response_tpl['status_code'] = 401
    response_tpl['status_text'] = 'Unauthorized'
    response_tpl['data'] = None
    response_tpl['errors'] = [{"msg": "Must authenticate first"}]
    assert_equals(
        WSResponse.unauthorized([{"msg": "Must authenticate first"}]),
        response_tpl
    )


def test_response_not_found():
    """Testing WSResponse class for HTTP 404 ``Not Found`` status.
    """
    response_tpl['status_code'] = 404
    response_tpl['status_text'] = 'Not Found'
    response_tpl['data'] = None
    response_tpl['errors'] = [{"msg": "Resource not found."}]
    assert_equals(WSResponse.not_found([{"msg": "Resource not found."}]), response_tpl)


def test_response_method_not_allowed():
    """Testing WSResponse class for HTTP 405 ``Method Not Allowed`` status.
    """
    response_tpl['status_code'] = 405
    response_tpl['status_text'] = 'Method Not Allowed'
    response_tpl['data'] = None
    response_tpl['errors'] = [{"msg": "Can't use PUT method."}]
    assert_equals(
        WSResponse.method_not_allowed([{"msg": "Can't use PUT method."}]),
        response_tpl
    )


def test_response_not_implemented():
    """Testing WSResponse class for HTTP 501 ``Not Implemented`` status.
    """
    response_tpl['status_code'] = 501
    response_tpl['status_text'] = 'Not Implemented'
    response_tpl['data'] = None
    response_tpl['errors'] = [{"msg": "Soon will be available"}]
    assert_equals(
        WSResponse.not_implemented([{"msg": "Soon will be available"}]),
        response_tpl
    )


def test_response_service_unavailable():
    """Testing WSResponse class for HTTP 503 ``Service Unavailable`` status.
    """
    response_tpl['status_code'] = 503
    response_tpl['status_text'] = 'Service Unavailable'
    response_tpl['data'] = None
    response_tpl['errors'] = [{"msg": "Currently down for maintenance."}]
    assert_equals(
        WSResponse.service_unavailable([{"msg": "Currently down for maintenance."}]),
        response_tpl
    )