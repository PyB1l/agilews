.. AgileWS documentation master file, created by
   sphinx-quickstart on Wed Nov 12 15:25:20 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to AgileWS's documentation!
===================================

Contents:

.. toctree::
   :maxdepth: 1

   tutorial
   response
   handlers



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

