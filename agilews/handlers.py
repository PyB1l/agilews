# -*- coding: utf-8 -*-
"""
Provides a class-based HTTP BaseHandler for bottle.py micro-framework. Subclass
the base handler, implement your *http-verb* corresponding methods, config
handler routing-specific attributes and your ready.

.. note::
    By default ``OPTIONS`` method is auto-generated on *routing* time,
    according to your method implementation. It fully supports bottle.py
    route-parameter system, but in order to access options method it you must
    invoce the routing funtion.


Example usage::

    >>> class ResourceHandler(BaseHTTPHandler):
    ...     # Example class-based Handler,
    ...     # Subclass Basehandler and config attributes.
    ...
    ...     endpoint = '/model/<pk:int>'
    ...     lookup = '<pk:int>'
    ...
    ...     # implement the methods
    ...     def get(self, pk):
    ...         return 'Get resource {}.'.format(pk)
    ...
    ...     def delete(self, pk):
    ...         return 'Deleted resource {}.'.format(pk)
    ...
    >>> print ResourceHandler.get(34)
    Get resource 34.
    >>> print ResourceHandler.delete(34)
    Deleted resource 34.


.. note::
    It is possible to remove ``lookup`` parameter from a specified method by
    setting ``skip_lookup`` attribute::

        >>> class TestHandler(BaseHTTPHandler):
        ...
        ...     endpoint = '/model/<pk:int>'
        ...     lookup = '<pk:int>'
        ...     skip_lookup = ['post']
        ...
        ...     # implement the methods
        ...     def get(self, pk):
        ...         return 'Get resource {}.'.format(pk)
        ...
        ...     def post(self):
        ...         return 'Created new resource.'
        ...
        >>> print TestHandler.handler_route
        {'post': '/model', 'get': '/model/<pk:int>'}


Module documentation
++++++++++++++++++++
"""

__version__ = '0.1'
__author__ = 'Papavassileiou Vassilis'
__license__ = 'GLPv3'
__all__ = ['BaseHTTPHandler']


import types


KNOWN_METHODS = ('get', 'post', 'options', 'put', 'delete', 'head', 'patch')


class BaseHTTPMeta(type):
    """**BaseHTTPHandler MetaClass**

    HTTPHandlerMeta class provides a core functionality in order
    to make derived classes methods ``bottle-routing`` friendly.

    .. note::

    """

    def __new__(mcs, name, bases, attrs):
        """Attaches routing info on every subclass of ``BaseHTTPHandler``.
        """
        mcs_methods = [attr_label for attr_label in attrs
                       if isinstance(attrs[attr_label], types.FunctionType) and
                       not attr_label.startswith('_')]

        assert set(mcs_methods).issubset(set(KNOWN_METHODS)),\
            'Invalid HTTP methods for {} Handler class.'.format(name)

        mcs_http = [m.upper() for m in mcs_methods]

        skip_lookup = attrs.get('skip_lookup') or []

        assert isinstance(skip_lookup, list), 'Invalid parameter: skip_lookup'

        lookup = attrs['lookup'] = attrs.get('lookup') or ''

        uri = attrs['endpoint'] = attrs.get('endpoint') or ''

        attrs['allowed_methods'] = staticmethod(lambda: mcs_http)

        attrs['handler_route'] = {}

        for label in mcs_methods:
            if label in skip_lookup:
                attrs['handler_route'][label] = uri.replace('/' + lookup, '')
            else:
                attrs['handler_route'][label] = uri
            attrs[label] = classmethod(attrs[label])
        return super(BaseHTTPMeta, mcs).__new__(mcs, name, bases, attrs)


class BaseHTTPHandler(object):
    """**Base HTTP handler class.**

    BaseHTTPHandler class provides a base class-based HTTP handler.

    Attributes:
        - **endpoint** (str)     - Handler base URI-endpoint.
        - **lookup** (str)       - Handler URI lookup bottle.py regex.
        - **skip_lookup** (list) - Method name-list to except ``lookup``.

    .. note::
        Subclasses of ``BaseHTTPHandler`` endpoint - methods must correspond to
        HTTP verbs in order auto-mapping to work. Extra methods for helper
        functionality can be implemented as long as method name start with one
        underscore (i.e ``_do_some_work``).
    """
    __metaclass__ = BaseHTTPMeta

    endpoint = ''
    lookup = ''
    skip_lookup = []


if __name__ == '__main__':
    import doctest
    doctest.testmod()
