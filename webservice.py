# -*- coding: utf-8 -*-
"""Testing web service bootstrap.
"""

__all__ = ['get_webservice']

import bottle

_webservice = None


def get_webservice():
    global _webservice

    if not _webservice:
        _webservice = bottle.app()
        print 'Initializing'
        return _webservice
    print 'Already exists'
    return _webservice
