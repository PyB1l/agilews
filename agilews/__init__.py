# -*- coding: utf-8 -*-
"""
**AgileWS**

The agile web service framework

::


.. note::
    In beta version only JSON renderer is supported. Must implement
    additional renderer classes (SOAP, XML, YAML).


Module documentation
++++++++++++++++++++
"""


__version__ = '0.1'
__author__ = 'Papavassileiou Vassilis'
__license__ = 'GLPv3'

__all__ = ['webservice', 'WSResponse', 'BaseHTTPHandler', 'json_validator',
           'error404', 'error405', 'error500', 'ws_request', 'ws_response',
           'agilews_run', 'cors_enable_hook', 'type_validator']

import bottle
webservice = bottle.app()

from bottle import (request as ws_request, response as ws_response, debug)
from response import *
from handlers import *
from errors import *
from routers import *
from utils import *
import default_settings


class StripPathMiddleware(object):
    """Ignore URI final slash and redirect to the same Resource:

    That means that:
            - http://service_one/resource/34
            - http://service_one/resource/34/

    are identical URI's.

    """
    def __init__(self, app):
        self.app = app

    def __call__(self, e, h):
        e['PATH_INFO'] = e['PATH_INFO'].rstrip('/')
        return self.app(e, h)


def agilews_run(app=None, settings_module='None', plugins=None, handlers=None):
    """Initialize WSGI server. You can declare a settings module with typical
    Bottle.py ``run`` function configuration, declare plugins to install,
    register routing handlers.

    Params:
        - **app**: webservice instance.
        - **settings_module**: webservice instance.
        - **plugins**: webservice instance.
        - **handlers**: webservice instance.

    Raises:
        bottle.py runtime Exceptions.
    """

    try:
        settings = __import__(settings_module)

    except ImportError:
        settings = default_settings

    debug(settings.DEBUG or False)

    app.hook('after_request')(cors_enable_hook)

    if plugins and isinstance(plugins, list):
        for plugin in plugins:
            app.install(plugin)

    if handlers and isinstance(handlers, list):
        for handler in handlers:
            handler_dispatch(bottle, handler)  # app.route

    bottle.route('/ping')(lambda: WSResponse.ok({'service': 'pong'}))
    bottle.route('/ping', method=['OPTIONS'])(lambda: WSResponse.ok(
        {'allowed_methods': ['GET']})
    )

    bottle.run(app=StripPathMiddleware(app), **settings.WSGI)
