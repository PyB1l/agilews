"""Example usage
"""

from agilews import (BaseHTTPHandler, WSResponse, agilews_run, json_validator)
from agilews import (webservice, ws_request, ws_response, type_validator)
import random


class ModelHandler(BaseHTTPHandler):
    """Map handler example
    """

    endpoint = '/model/:pk'
    lookup = ':pk'
    skip_lookup = ['post']

    def get(self, pk):
        return WSResponse.ok(
            data={
                'msg': 'Get resource id {}'.format(pk),
                'model_id': pk,
                'model_category': 'whatever',
                'model_rate': random.randint(0, pk)
            })

    @json_validator
    def put(self, pk):
        data = ws_request.json
        data.update({"alter_resource": "completed"})
        return WSResponse.ok(data=data)

    @json_validator
    def post(self):
        return WSResponse.ok(data=ws_request.json)

    def delete(self, pk):
        return WSResponse.ok(data={'msg': 'Delete resource id {}'.format(pk)})


if __name__ == '__main__':

    agilews_run(
        app=webservice,
        settings_module='example_settings',
        plugins=[],
        handlers=[ModelHandler]
    )