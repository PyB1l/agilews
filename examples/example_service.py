"""Example usage
"""

from agilews import (BaseHTTPHandler, WSResponse, agilews_run, json_validator)
from agilews import (webservice, ws_request)


class ModelHandler(BaseHTTPHandler):
    """Map handler example
    """

    endpoint = '/model/:pk/'

    def get(self, pk):
        return WSResponse.ok(data={'msg': 'Get resource id'.format(pk)})

    def put(self, pk):
        return WSResponse.ok(data={'msg': 'Alter resource id'.format(pk)})

    @json_validator
    def post(self):
        return WSResponse.ok(data={'msg': ws_request.json})

    def delete(self, pk):
        return WSResponse.ok(data={'msg': 'Delete resource id'.format(pk)})


if __name__ == '__main__':
    agilews_run(
        app=webservice,
        settings_module='example_settings',
        plugins=[],
        handlers=[ModelHandler]
    )